const fs = require('fs');
const express = require('express');
const morgan = require('morgan')
const path = require('path');
const app = express();

const { filesRouter } = require('./filesRouter.js');

app.use(express.json());

const accessLogStream = fs.createWriteStream(path.join(__dirname, 'logs.log'), { flags: 'a' });

app.use(morgan('combined', { stream: accessLogStream }));

app.use('/api/files', filesRouter);

const start = async () => {
  try {
    if (!fs.existsSync('files')) {
      fs.mkdirSync('files');
    }
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
}

start();

app.use((req, res, next) => {
  next({status: 404, 'message': 'Not found'});
})

//ERROR HANDLER
app.use(errorHandler)

function errorHandler (err, req, res, next) {
  if (err.status === 400 || err.status === 404) {
    res.status(err.status).send({'message': err.message});
  }

  res.status(500).send({'message': 'Server error'});
}
